## FreeCAD docker build environment

This is a docker container to compile FreeCAD from source, based on an Arch-based container that uses all the precompiled binaries from the default repositories.

----

The directories containing FreeCAD's source code and build are not included
inside the docker image. Instead, they are attached to the docker container
when you run the container. This allows the built code to have continuity
across different docker containers, reducing the time for a build to occur, and
allowing you to use your own editor/IDE outside of the container.

# Image use

## Pull image

```
docker pull registry.gitlab.com/bonnee/freecad-build-env:latest
```

## Build FreeCAD

Using enviroment variables, specify:

- The root directory of the FreeCAD source;
- Where to build FreeCAD;
- An install directory, where freecad will be installed

Run the docker image.

```
docker run --rm \
-v /path/to/src:/mnt/source \
-v /path/to/build:/mnt/build \
-v /path/to/install:/mnt/install \
registry.gitlab.com/bonnee/freecad-build-env:latest
```

You will be able to find the mounted directories within the container in the
`/mnt` directory, named `/mnt/source`, `/mnt/build`, and `/mnt/install`.

## Build docker image

```
docker build -t docker.io/bonnee/freecad-build-env:latest
```


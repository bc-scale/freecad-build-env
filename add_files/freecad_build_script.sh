#!/bin/bash

set -e

#cmake -DBUILD_QT5=ON -DPYTHON_EXECUTABLE=/usr/bin/python3 -DCMAKE_BUILD_TYPE=Release -S /mnt/source -B /mnt/build
cmake -GNinja -Wno-dev .. \
        -DBUILD_QT5=ON \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_DATADIR='/usr/share/freecad' \
        -DCMAKE_INSTALL_DOCDIR='/usr/share/freecad/doc' \
        -DCMAKE_INSTALL_PREFIX='/usr/lib/freecad' \
        -DFREECAD_USE_OCC_VARIANT="Official Version" \
        -DFREECAD_USE_EXTERNAL_PIVY=ON \
        -DFREECAD_USE_QT_FILEDIALOG=ON \
        -DPYTHON_EXECUTABLE=/usr/bin/python \
	-S /mnt/source \
	-B /mnt/build

cd /mnt/build

ninja -j$(($(nproc)-1))

DESTDIR="/mnt/install" ninja install


